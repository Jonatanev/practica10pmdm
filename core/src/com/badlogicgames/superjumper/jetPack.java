package com.badlogicgames.superjumper;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Jonatan on 13/04/2017.
 */

public class jetPack extends GameObject {
    public static float JETPACK_WIDTH = 1.6f;
    public static float JETPACK_HEIGHT = 1.6f;

    public jetPack(float x, float y) {

        super(x,y,JETPACK_WIDTH, JETPACK_HEIGHT);
    }
}
